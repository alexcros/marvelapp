//
//  MarvelEntityTests.swift
//  MarvelAppTests
//
//  Created by Alexandre on 20/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import XCTest
@testable import MarvelApp

class MarvelEntityTests: XCTestCase {
    
    var charactersResponse: CharactersResponse?
    
    override func setUp() {
        super.setUp()
        
        guard let path = Bundle(for: type(of: self)).path(forResource: "characters-response", ofType: "json") else {
            XCTFail("path or file error")
            return
        }
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path))
            charactersResponse = try JSONDecoder().decode(CharactersResponse.self, from: data)
            
            
        } catch {
            XCTFail("Error decoding data")
        }
    }
    
    func testDecodeMarvelCharactersResponse() {
        XCTAssertNotNil(charactersResponse)
    }
    
    func testMarvelCharactersResponse() {
        XCTAssertNotNil(charactersResponse)
    }
    
    func testDecodeMarvelCharactersCollection() {
        guard let marvelCollection = charactersResponse?.data.characters else {
            XCTFail()
            return
        }
        XCTAssertTrue(marvelCollection.count > 1)
        
        for character in marvelCollection {
            XCTAssertNotNil(character.description)
            XCTAssertNotNil(character.id)
            XCTAssertNotNil(character.name)
            XCTAssertNotNil(character.thumbnail)
        }
    }
}
