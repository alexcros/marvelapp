//
//  MarvelCollectionInteractorTests.swift
//  MarvelAppTests
//
//  Created by Alexandre on 20/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import XCTest
@testable import MarvelApp

class MarvelCollectionInteractorTests: XCTestCase {

    var subject: MarvelCollectionInteractor!
    let client = FakeClient(configuration: .default)
    let marvelCollectionInteractorOutput = FakeInteractionOutput()

    override func setUp() {
        super.setUp()
        subject = MarvelCollectionInteractor(client: client)
        subject.output = marvelCollectionInteractorOutput
    }

    private func fetchMarvelCharactersWithSuccess() {
        client.result = StubMarvelCollectionResult.successfulResult
        subject.loadCharacters(with: .marvelCollection)
    }
    
    private func loadMarvelCharactersInSubjectWithError() {
        let errorResult = StubMarvelCollectionResult.errorResult
        client.result = errorResult
        subject.loadCharacters(endPoint: .marvelCollection)
    }
    
    struct StubMarvelCollectionResult {
        static let errorResult: Result<[MarvelCharacter]> = Result.failure(.noConnection)
        static let successfulResult: Result<[MarvelCharacter]> = Result.success(WebService.characters)
    }
    
    func testLoadMarvelCharacterWithSuccess_CharactersAreCaughtInSubjectOutputProtocol() {
        // success
        fetchMarvelCharactersWithSuccess()
        
        // Catch Characters in Output Protocol
        guard let marvelCharactersInInteractionOutputProtocol = marvelCollectionInteractorOutput.characters else {
            XCTFail("Output without marvel characters")
            return
        }
        switch StubMarvelCollectionResult.successfulResult {
        case .success(let characters):
            XCTAssertEqual(characters, marvelCharactersInInteractionOutputProtocol)
        default:
            XCTFail("success: test never reach it")
        }

    }

    class FakeInteractionOutput: MarvelCollectionInteractionOutput {
        var characters: [MarvelCharacter]?
        var error: String?

        func refreshMarvelCollection(with characters: [MarvelCharacter]) {
            self.characters = characters
        }

        func showLoadingMarvelListError(_ error: String) {
            self.error = error
        }

    }

    class FakeClient: WebService {
        var result: Result<[MarvelCharacter]>?

        func getMarvelCollection(from endpoint: ApiEndpoint, callback: (Result<[MarvelCharacter]>) -> Void) {
            guard let result = result else {
                XCTFail("fake result not provided")
                return
            }
            callback(result)
        }
    }

}

extension MarvelCollectionInteractor {
    // MARK: - Fake WebService
    func loadCharacters(with endPoint: ApiEndpoint) {
           client.getMarvelCollection(from: endPoint) { (result) in
               output?.refreshMarvelCollection(with: result)
           }
       }
}
