//
//  MarvelCollectionPresenterTests.swift
//  MarvelAppTests
//
//  Created by Alexandre on 21/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import XCTest
@testable import MarvelApp

class MarvelCollectionPresenterTests: XCTestCase {
    
    var presenter: MarvelCollectionPresenter?
    let mockInteractor = MockInteractor(client: WebService(configuration: .default))
    let mockRouter = MockRouter()
    let characters = [MarvelCharacter(id: 3, description: "", name: "", thumbnail: .init(path: "", thumbnailExtension: .jpg))]
    var mockInterface: MockInterface?
    
    override func setUp() {
        super.setUp()
        
        presenter = MarvelCollectionPresenter(interactor: mockInteractor, router: mockRouter, marvelClient: WebService(configuration: .default))
        mockInterface = MockInterface()
        presenter?.marvelCollectionViewInterface = mockInterface
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testMarvelCharacterIsInjected() {
        presenter?.refreshMarvelCollection(with: characters)
        let characterTest = presenter?.character(at: 0)
        XCTAssertEqual(characterTest, characters.first)
    }
    
    func testMarvelCollectionEmpty_ShouldShowError() {
        presenter?.refreshMarvelCollection(with: [])
        XCTAssertEqual(mockInterface?.shouldShowError, true)
        XCTAssertEqual(mockInterface?.shouldLoadMarvelCollectionCharacter, false)
    }
    
    func testMarvelCollectionWithCharacters_ShouldFetchCharacters() {
        presenter?.refreshMarvelCollection(with: characters)
        XCTAssertEqual(mockInterface?.shouldLoadMarvelCollectionCharacter, true)
        XCTAssertEqual(mockInterface?.shouldShowError, false)
    }
    
    class MockInteractor: MarvelCollectionInteractor {
        
        override func loadCharacters(endPoint: ApiEndpoint) {
        }
        
    }
    
    class MockInterface: MarvelCollectionViewInterfaceProtocol {
        
        var shouldLoadMarvelCollectionCharacter = false
        var shouldShowError = false
        
        func refreshCharactersCollection() {
            shouldLoadMarvelCollectionCharacter = true
        }
        
        func showLoadingError(errorMessage: String) {
            shouldShowError = true
        }
    }
    
    class MockRouter: MarvelCollectionRoutingProtocol {
        var detailDependencyContainer: MarvelDetailDependencyContainer = MarvelDetailDependencyContainer()
        var character: MarvelCharacter?
        
        func presentMarvelDetailView(with character: MarvelCharacter) {
            self.character = character
        }
     }
    
}
