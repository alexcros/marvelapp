//
//  SceneDelegate.swift
//  MarvelApp
//
//  Created by Alexandre on 20/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(frame: UIScreen.main.bounds)
        let core = DependencyModule()
        window?.rootViewController = UINavigationController(rootViewController: core.createMarvelCollectionMainModule())
        window?.makeKeyAndVisible()
        window?.windowScene = windowScene
    }
}
