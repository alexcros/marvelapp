//
//  CharactersResponse.swift
//  MarvelApp
//
//  Created by Alexandre on 20/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

// MARK: - CharactersResponse
struct CharactersResponse: Codable {
    let data: DataClass
}
