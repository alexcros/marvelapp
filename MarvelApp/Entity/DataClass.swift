//
//  DataClass.swift
//  MarvelApp
//
//  Created by Alexandre on 20/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

// MARK: - DataClass
struct DataClass: Codable {
    let characters: [MarvelCharacter]
    
    private enum CodingKeys: String, CodingKey {
        case characters = "results"
    }
}
