//
//  Thumbnail.swift
//  MarvelApp
//
//  Created by Alexandre on 20/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

// MARK: - Extension type
enum Extension: String, Codable {
    case gif = "gif"
    case jpg = "jpg"
}

// MARK: - Thumbnail
struct Thumbnail: Codable {
    let path: String
    let thumbnailExtension: Extension
    
    private enum CodingKeys: String, CodingKey {
        case path
        case thumbnailExtension = "extension"
    }
}

