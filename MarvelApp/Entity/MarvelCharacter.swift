//
//  MarvelCharacter.swift
//  MarvelApp
//
//  Created by Alexandre on 20/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

// MARK: - Result
struct MarvelCharacter: Codable {
    let id: Int
    let description, name: String
    let thumbnail: Thumbnail
    
    init(id: Int, description:String, name: String, thumbnail: Thumbnail) {
        self.id = id
        self.description = description
        self.name = name
        self.thumbnail = thumbnail
    }
}

// MARK: - Private methods
private extension MarvelCharacter {
    enum CodingKeys: String, CodingKey {
        case id, name, description
        case thumbnail
    }
}

// MARK: - Equatable
extension MarvelCharacter: Equatable {
    static func ==(lhs: MarvelCharacter, rhs: MarvelCharacter) -> Bool {
        return lhs.name == rhs.name &&
            lhs.id == rhs.id &&
            lhs.description == rhs.description
    }
}
