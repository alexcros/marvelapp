//
//  MarvelResult.swift
//  MarvelApp
//
//  Created by Alexandre on 20/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(MarvelServiceError)
}

enum MarvelServiceError: Error {
    case badStatus(Int, Data)
    case api(Int, String)
    case noConnection
}

struct Status: Decodable {
    let code: Int
    let message: String
    
    enum CodingKeys: String, CodingKey {
        case code = "status_code"
        case message = "status_message"
    }
}


