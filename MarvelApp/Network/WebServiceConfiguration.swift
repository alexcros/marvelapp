//
//  WebServiceConfiguration.swift
//  MarvelApp
//
//  Created by Alexandre on 21/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

struct WebServiceConfiguration {
    let hash: String
    let apiKey: String
    let ts: String
    
    static let `default` = WebServiceConfiguration(hash: "d3870da02003a598e98945cdbc7a6485",
                                                   apiKey: "ca19ce135900ba9ab2c863f912777856",
                                                   ts: "1")
    var parameters: [String: String] {
        return ["hash": hash, "apikey": apiKey, "ts": ts]
    }
}
