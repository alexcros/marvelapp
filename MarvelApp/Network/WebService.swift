//
//  WebService.swift
//  MarvelApp
//
//  Created by Alexandre on 20/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

protocol MarvelCollectionProtocol: class {
    func getMarvelCollection(from endpoint: ApiEndpoint, callback: (_ result: [MarvelCharacter]) -> Void)
}

class WebService: MarvelCollectionProtocol {
    static var shared = WebService(configuration: .default)
    private let baseUrl = URL(string: AppConstants.baseURL)!
    private let session = URLSession(configuration: .default)
    private let decoder = JSONDecoder()
    private let configuration: WebServiceConfiguration
    
    init(configuration: WebServiceConfiguration) {
        self.configuration = configuration
    }
    
    func getMarvelCollection(from endpoint: ApiEndpoint, callback: ([MarvelCharacter]) -> Void) {
        let result = WebService.characters
        callback(result)
    }
}

extension WebService {
    
    static let characters: [MarvelCharacter] = {
        return [
            MarvelCharacter(id: 1, description: "", name: "3D-Man", thumbnail: .init(path: "", thumbnailExtension: .jpg)),
            MarvelCharacter(id: 2, description: "", name: "IronMAn", thumbnail: .init(path: "", thumbnailExtension: .gif))
        ]
    }()
}

extension WebService {
    
    // MARK: - Marvel Characters Service
    func getCharacters(completion: @escaping (CharactersResponse?, Error?) -> Void) {
        let request = ApiEndpoint.marvelCollection.request(with: baseUrl, adding: configuration.parameters)
        session.dataTask(with: request) { (data, _, error) in
            if (error != nil) {
                print(error!.localizedDescription)
            }
            guard let data = data,
                let response = try? self.decoder.decode(CharactersResponse.self, from: data) else {
                    if let error = error {
                        completion(nil, error)
                    }
                    return
            }
            completion(response, nil)
        }.resume()
    }
}
