//
//  AppConstants.swift
//  MarvelApp
//
//  Created by Alexandre on 21/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

enum imageSize: String {
    case portraitSmall = "portrait_small"
    case standardMedium = "standard_medium"
    case standardFantastic = "portrait_medium"
}

struct AppConstants {
    static let baseURL = "https://gateway.marvel.com/v1/public"
    static let publicKey = "ca19ce135900ba9ab2c863f912777856"
}


