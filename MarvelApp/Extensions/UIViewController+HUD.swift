//
//  UIViewController+HUD.swift
//  MarvelApp
//
//  Created by Alexandre on 21/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit
import MBProgressHUD

extension UIViewController {
    
    func showIndicator(withTitle title: String, and text: String) {
        let indicator = MBProgressHUD.showAdded(to: self.view, animated: true)
        indicator.label.text = title
        indicator.isUserInteractionEnabled = false
        indicator.detailsLabel.text = text
        indicator.show(animated: true)
    }
    
    func showIndicator() {
        showIndicator(withTitle: NSLocalizedString("Loading", comment: ""), and: NSLocalizedString("Please wait...", comment: ""))
    }
    
    func hideIndicator() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
