//
//  UIViewExtension.swift
//  MarvelApp
//
//  Created by Alexandre on 21/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit

extension UIView {
    func addSubViewList(_ view: UIView...) {
        view.forEach { self.addSubview($0) }
    }
}
