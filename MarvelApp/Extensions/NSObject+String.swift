//
//  NSObject+String.swift
//  MarvelApp
//
//  Created by Alexandre on 21/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

extension NSObject {
    class var name: String {
        return "\(self.self)"
    }
}
