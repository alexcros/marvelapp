//
//  DependencyModule.swift
//  MarvelApp
//
//  Created by Alexandre on 20/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit

final class DependencyModule {
    private(set) lazy var interactor = MarvelCollectionInteractor()
    private(set) lazy var presenter = MarvelCollectionPresenter(interactor: interactor, router: router)
    private(set) lazy var router = MarvelCollectionRouter()
    
    func createMarvelCollectionMainModule() -> MarvelCollectionController {
        
        let marvelCollectionController = MarvelCollectionController(collectionViewLayout: UICollectionViewFlowLayout())
        marvelCollectionController.presenter = presenter
        presenter.marvelCollectionViewInterface = marvelCollectionController
        interactor.output = presenter
        
        return marvelCollectionController
    }
}

