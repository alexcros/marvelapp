//
//  MarvelCollectionCell.swift
//  MarvelApp
//
//  Created by Alexandre on 21/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Kingfisher

class MarvelCollectionCell: UICollectionViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var marvelImageView: UIImageView!
    
    func setupMarvelCell(with character: MarvelCharacter?) {
        setupCellLayer()
        
        //TODO: IMPLEMENT IT

    }
    
}

private extension MarvelCollectionCell {
    func setupCellLayer() {
        layer.cornerRadius = 10
        layer.masksToBounds = true
    }
}
