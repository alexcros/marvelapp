//
//  MarvelCollectionController.swift
//  MarvelApp
//
//  Created by Alexandre on 21/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Kingfisher

enum Constants {
    static let edges: CGFloat = 8
    static let columns: CGFloat = 2
    static let spacing: CGFloat = 8
}

class MarvelCollectionController: UICollectionViewController {
    
    var presenter: MarvelCollectionPresentationProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = NSLocalizedString("MarvelApp", comment: "")
        setupCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter?.loadCharacters()
    }
    
    func setupCollectionView() {
         let marvelCell = UINib(nibName: "MarvelCollectionCell", bundle: nil)
         collectionView.register(marvelCell, forCellWithReuseIdentifier: MarvelCollectionCell.name)
         collectionView.showsVerticalScrollIndicator = true
     }
    
}

extension MarvelCollectionController: MarvelCollectionViewInterfaceProtocol {
    // TODO: Implement pull to refresh
    func refreshCharactersCollection() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func showLoadingError(errorMessage: String) {
        print("error: \(errorMessage.localizedUppercase)")
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension MarvelCollectionController: UICollectionViewDelegateFlowLayout {
    
    // MARK: - DataSource methods
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter?.characterCount ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MarvelCollectionCell.name,
                                                            for: indexPath) as? MarvelCollectionCell,
            let character =  presenter?.character(at: indexPath.item) else { return UICollectionViewCell() }
        
        // cell.setupMarvelCell(with: character)
        let imagePath = "\(String(describing: character.thumbnail.path))/\(imageSize.standardFantastic.rawValue).\(String(describing: character.thumbnail.thumbnailExtension.rawValue.self))"
        
        cell.nameLabel.text = character.name
        cell.marvelImageView.kf.setImage(with: URL(string: imagePath))
        cell.backgroundColor = .darkGray
        
        return cell
    }
    
    // MARK: - Delegate methods
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let character = presenter?.character(at: indexPath.item) {
            presenter?.selectCharacter(character)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let totalSpacing = (2 * Constants.spacing) + ((Constants.columns) * Constants.spacing)

        let width = (collectionView.bounds.width - totalSpacing) / Constants.columns
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: Constants.edges, left: Constants.edges, bottom: Constants.edges, right: Constants.edges)
    }
}
