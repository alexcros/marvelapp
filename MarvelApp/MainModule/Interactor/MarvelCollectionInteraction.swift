//
//  MarvelCollectionInteraction.swift
//  MarvelApp
//
//  Created by Alexandre on 20/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

// MARK:- Interaction Protocol
protocol MarvelCollectionInteractionProtocol {
    func loadCharacters(endPoint: ApiEndpoint)
}
