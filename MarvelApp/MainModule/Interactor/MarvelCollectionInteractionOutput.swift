//
//  MarvelCollectionInteractionOutput.swift
//  MarvelApp
//
//  Created by Alexandre on 20/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

// MARK:- Interaction -> Presenter Protocol
protocol MarvelCollectionInteractionOutput: class {
    var characters: [MarvelCharacter]? { get }
    func refreshMarvelCollection(with characters: [MarvelCharacter])
    func showLoadingMarvelListError(_ error: String)
}
