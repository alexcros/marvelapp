//
//  MarvelCollectionInteractor.swift
//  MarvelApp
//
//  Created by Alexandre on 20/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

// MARK:- Interactor
class MarvelCollectionInteractor: MarvelCollectionInteractionProtocol {
    
    var characters: [MarvelCharacter]?
    weak var output: MarvelCollectionInteractionOutput?
    
    /// Init
    /*private*/ let client: WebService
    
    init(client: WebService = WebService(configuration: .default) ) {
        self.client = client
    }
    /// Get characters from webservice
    /// On success: refresh characters list in collectionview
    /// On failure: show error view in collectionview
    func loadCharacters(endPoint: ApiEndpoint) {
        client.getCharacters { response, error in
            guard let marvelCollection = response?.data.characters else {
                self.output?.showLoadingMarvelListError(error?.localizedDescription ?? "Error loading characters")
                return
            }
            DispatchQueue.main.async {
                self.output?.refreshMarvelCollection(with: marvelCollection)
            }
        }
    }
}
