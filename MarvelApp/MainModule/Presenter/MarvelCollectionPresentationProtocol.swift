//
//  MarvelCollectionPresentationProtocol.swift
//  MarvelApp
//
//  Created by Alexandre on 21/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

// MARK: - Collection View Presentation Protocol
protocol MarvelCollectionPresentationProtocol: class {
    var characterCount: Int { get }
    func character(at index: Int) -> MarvelCharacter?
    func loadCharacters()
    func selectCharacter(_ character: MarvelCharacter)
}
