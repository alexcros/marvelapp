//
//  MarvelCollectionViewInterfaceProtocol.swift
//  MarvelApp
//
//  Created by Alexandre on 21/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

// MARK: - Presenter -> View Interface Protocol
protocol MarvelCollectionViewInterfaceProtocol: class {
    func refreshCharactersCollection()
    func showLoadingError(errorMessage: String)
}
