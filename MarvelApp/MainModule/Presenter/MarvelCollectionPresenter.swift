//
//  MarvelCollectionPresenter.swift
//  MarvelApp
//
//  Created by Alexandre on 20/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

// MARK: - Presenter
class MarvelCollectionPresenter {
    
    // MARK: - Interface
    weak var marvelCollectionViewInterface: MarvelCollectionViewInterfaceProtocol?
    
    // MARK: Init
    private var interactor: MarvelCollectionInteractionProtocol
    private let router: MarvelCollectionRoutingProtocol
    private let marvelClient: WebService
    
    init(interactor: MarvelCollectionInteractionProtocol,
         router: MarvelCollectionRoutingProtocol,
         marvelClient: WebService = WebService(configuration: .default)) {
        self.interactor = interactor
        self.router = router
        self.marvelClient = marvelClient
    }
    
    private(set) var characters: [MarvelCharacter]? {
        didSet {
            guard let characters = characters, !characters.isEmpty else {
                marvelCollectionViewInterface?.showLoadingError(errorMessage: "No character Loaded")
                return
            }
            marvelCollectionViewInterface?.refreshCharactersCollection()
        }
    }
}

extension MarvelCollectionPresenter: MarvelCollectionPresentationProtocol {
    
    var characterCount: Int {
        return characters?.count ?? 0
    }
    
    func character(at index: Int) -> MarvelCharacter? {
        return characters?[index] ?? nil
    }
    
    func loadCharacters() {
        interactor.loadCharacters(endPoint: .marvelCollection)
    }
    
    func selectCharacter(_ character: MarvelCharacter) {
        router.presentMarvelDetailView(with: character)
    }
}

extension MarvelCollectionPresenter: MarvelCollectionInteractionOutput {
    
    func refreshMarvelCollection(with characters: [MarvelCharacter]) {
        self.characters = characters
    }
    
    func showLoadingMarvelListError(_ error: String) {
        marvelCollectionViewInterface?.showLoadingError(errorMessage: error.localizedUppercase)
    }
}
