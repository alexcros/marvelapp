//
//  MarvelCollectionRouter.swift
//  MarvelApp
//
//  Created by Alexandre on 20/10/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

typealias MarvelDetailDependencyContainer = String

protocol MarvelCollectionRoutingProtocol: class {
    var detailDependencyContainer: MarvelDetailDependencyContainer  { get set }
    func presentMarvelDetailView(with character: MarvelCharacter)
}
/*Router*/
class MarvelCollectionRouter: MarvelCollectionRoutingProtocol {
    var detailDependencyContainer = MarvelDetailDependencyContainer()
    var mainDependencyContainer = DependencyModule()
    
    func presentMarvelDetailView(with character: MarvelCharacter) {
        // create detail module
    }
    
}

